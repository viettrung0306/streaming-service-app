import "./App.css";
import LoginPage from "./pages/LoginPage";
import { Routes, Route, Link, Navigate, useNavigate } from "react-router-dom";
import HomePage from "./pages/HomePage";
import MoviePage from "./pages/MoviePage";
import ProfilePage from "./pages/Profile";
import { FavoritesProvider } from "./components/favorite/Favorite";

const About = () => {
  return (
    <>
      <main>
        <h2>Who are we?</h2>
        <p>That feels like an existential question, don't you think?</p>
      </main>
      <nav>
        <Link to="/">Home</Link>
      </nav>
    </>
  );
}

const App = () => {
  return (
    <div className="App">
      <FavoritesProvider>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route
            path="/browse/*"
            element={
              localStorage.getItem("accessToken") ? (
                <HomePage />
              ) : (
                <Navigate to="/" replace="true" />
              )
            }
          />
          <Route path="/browse/:id/:name" element={<MoviePage />} />
          <Route path="/profile" element={<ProfilePage />} />
        </Routes>
      </FavoritesProvider>
    </div>
  );
}

export default App;
