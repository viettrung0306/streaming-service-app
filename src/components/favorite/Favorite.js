import React, { useState, useCallback } from "react";

export const FavoritesContext = React.createContext({ favorites: [] });

export const FavoritesProvider = ({ children }) => {
  const [favorites, setFavorites] = useState([]);
  const add1 = (favorite) => {
    setFavorites((current) => {
      console.log("Before IF: ", current);
      if (current.some((film) => film.id === favorite.id)) {
        let final = [];
        let index = current.indexOf(favorite);
        console.log(current);
        current.length > 2 ? (final = current.splice(index, 1)) : current.pop();
        console.log("Remove success: new arr =", current);
        localStorage.setItem(
          "favorite",
          current.map((fav) => fav.id),
        );
        return current;
        // remove it
      } else {
        let final = [...current, favorite];
        console.log("Add new Film: ", final);
        localStorage.setItem(
          "favorite",
          final.map((fav) => fav.id),
        );
        console.log("Add Final result: ", final);
        return final;
        // add it
      }
    });
  };
  return (
    <FavoritesContext.Provider value={{ favorites, add1 }}>
      {children}
    </FavoritesContext.Provider>
  );
};
