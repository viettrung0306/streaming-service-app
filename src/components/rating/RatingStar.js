import React from "react";
import ReactStars from "react-rating-stars-component";

const Rating = ({ rating }) => {
  const thirdExample = {
    size: 35,
    count: 10,
    isHalf: true,
    value: rating,
    color: "black",
    activeColor: "red"
  };
  return <ReactStars {...thirdExample} />;
};

export default Rating;
