import { useEffect } from "react";
import "./Cast.scss";
import _ from "lodash";

const Cast = ({ casts }) => {
  useEffect(() => {
    console.log(casts);
  }, []);

  return (
    <>
      {casts.map((cast) => {
        return (
          <div className="cast__card" key={_.get(cast,"person.id")}>
            <img src={_.get(cast, "character.image.medium", "")} className="cast__card--image" />
            <div className="cast__card--info">
              <p>
                <strong>{_.get(cast, "person.name", "")}</strong> as{" "}
                <strong>{_.get(cast, "character.name", "")}</strong>
              </p>
              <p>
                <strong>Birthday: </strong>
                {_.get(cast, "person.birthday", "")}
              </p>
              <p>
                <strong>Country: </strong>
                {_.get(cast, "person.country.name", "")}
              </p>
              <p>
                <strong>
                  <a href={_.get(cast, "person.url", "")}>More Information</a>
                </strong>
              </p>
            </div>
          </div>
        );
      })}
    </>
  );
};

export default Cast;
