import React, { useContext, useState, useCallback, useEffect, Children } from "react";
import { FavoritesContext } from "../favorite/Favorite";
import { useNavigate } from "react-router-dom";
import _ from "lodash";

const LikeBtn = ({ showInfo, handleAdd, liked }) => {
  {
    return liked === true ? (
      <span
        id={`likeBtn-${showInfo.id}`}
        className={`footer__like liked`}
        onClick={() => handleAdd(showInfo)}
      >
        ♥
      </span>
    ) : (
      <span
        id={`likeBtn-${showInfo.id}`}
        className={`footer__like`}
        onClick={() => handleAdd(showInfo)}
      >
        ♥
      </span>
    );
  }
};

export default function MovieCard({ show, children }) {
  const { favorites, add1 } = useContext(FavoritesContext);
  const [demoList, setDemoList] = useState(favorites);
  let navigate = useNavigate();

  useEffect(() => {
    setDemoList(localStorage.getItem("favorite"));
    // console.log('Udate Demolist: ', demoList);
  }, [demoList]);

  useEffect(() => {
    let favoriteList = localStorage.getItem("favorite");
    favoriteList === undefined && localStorage.setItem("favorite", "");
    console.log(1);
  }, []);

  const showFavorites = () => {
    localStorage.setItem(
      "favorite",
      favorites.map((fav) => fav.id),
    );
  };

  const handleAdd = (film) => {
    add1(film);
    const likeBtn = document.getElementById(`likeBtn-${film.id}`);
    likeBtn.classList.toggle("liked");
    // showFavorites();
  };

  const handleClickOnCard = (show) => {
    console.log("Clicked");
    localStorage.setItem("id", show.id);
    console.log("Favorites: ", favorites);
    navigate(`/browse/${show.id}/${show.name}`, { state: favorites });
  };

  return (
    <div className="card">
      <div className="image-wrapper" >
        <img src={_.get(show, "image.medium", "")} onClick={() => handleClickOnCard(show)}/>
      </div>
      <div className="card__content" >
        <p onClick={() => handleClickOnCard(show)}>{_.get(show, "name", "")}</p>
        {children}
      </div>
      <div className="card__footer">
        <LikeBtn
          showInfo={show}
          handleAdd={handleAdd}
          liked={demoList.includes(show.id)}
        />
        <span className="footer__rating">
          {_.get(show, "rating.average", "")}
        </span>
      </div>
    </div>
  );
}
