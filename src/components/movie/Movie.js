import React, { useState, useEffect, useContext } from "react";
import Rating from "../rating/RatingStar";
import axios from "axios";
import Cast from "../cast/Cast";
import { FavoritesContext } from "../favorite/Favorite";
import _ from "lodash";

const Movie = ({ movie }) => {
  const {
    name,
    image,
    summary,
    network,
    status,
    webChannel,
    schedule,
    averageRuntime,
    type,
    genres,
    rating,
  } = movie;

  const [video, setVideo] = useState(`${name} TRAILER OFFICIAL`);
  const [videoUrl, setVideoUrl] = useState();
  const [casts, setCasts] = useState([]);
  const { favorites, add1 } = useContext(FavoritesContext);
  const [favorite, setFavorite] = useState(false);

  useEffect(async () => {
    getCasts(movie.id);
    favorites.forEach((fav) => {
      let { id } = fav;
      id == movie.id ? setFavorite(true) : setFavorite(false);
    });
  }, []);

  useEffect(() => {
    console.log("1: ", favorites);
  }, [favorites]);

  function summaryDiv() {
    return { __html: `${summary}` };
  }

  async function getCasts(id) {
    const res = await axios.get(
      `https://api.tvmaze.com/shows/${id}?embed=cast`,
    );
    setCasts(res.data._embedded.cast);
    console.log(res.data._embedded.cast);
  }

  const handlerAddFavorite = (movie) => {
    add1(movie);
    setFavorite(!favorite);
  };

  return (
    <>
      <section id="movie__info" className="movie__info">
        <h1>{name}</h1>
        <div className="movie__info--script" id="movie__script">
          <aside>
            <img src={image.medium} />
              <button className="add-btn" onClick={() => handlerAddFavorite(movie)}>
                {favorite ? 'Remove from Favorites' : '♥ Add to Favorites'}
              </button>
          </aside>
          <div className="movie__info--block">
            {network !== null ? (
              <>
                <div className="box-info">
                  <h3>Show Info</h3>
                  <p>
                    <strong>Network: </strong>{" "}
                    <img
                      className="flag-align"
                      src={`https://static.tvmaze.com/intvendor/flags/${network.country.code.toLowerCase()}.png`}
                      alt="United States"
                      title="United States"
                    />{" "}
                    <a href="/networks/2/cbs">{network.name}</a>
                  </p>
                  <div>
                    <strong>Schedule:</strong> {schedule.days} at{" "}
                    {schedule.time} ({averageRuntime} min)
                  </div>
                  <div>
                    <strong>Status:</strong> {status}
                  </div>
                  <div>
                    <strong>Show Type:</strong> {type}
                  </div>
                  <div>
                    <strong>Genres:</strong>{" "}
                    <span className="divider">{genres.toString()}</span>
                  </div>
                  <div>
                    <strong>Official site:</strong>
                    <a href={_.get(network, "officialSite", "")}>
                      {_.get(network, "officialSite", "")}
                    </a>
                  </div>
                </div>
                <div className="box-rating">
                  <h3>
                    Rating: <span>{rating.average}</span>
                  </h3>
                  <Rating rating={rating.average} />{" "}
                </div>
              </>
            ) : (
              <>
                <div className="box-info">
                  <h3>Show Info</h3>
                  <p>
                    <strong>Web channel: </strong>{" "}
                    <a href="/networks/2/cbs">{webChannel.name}</a>
                  </p>
                  {averageRuntime !== null && (
                    <>
                      <strong>Average Runtime:</strong> {averageRuntime} minutes
                    </>
                  )}
                  <div></div>
                  <div>
                    <strong>Status:</strong> {status}
                  </div>
                  <div>
                    <strong>Show Type:</strong> {type}
                  </div>
                  <div>
                    <strong>Genres:</strong>{" "}
                    <span className="divider">{genres.toString()}</span>
                  </div>
                  <div>
                    <strong>Official site: </strong>
                    <a href={webChannel.officialSite}>
                      {webChannel.officialSite}
                    </a>
                  </div>
                </div>

                <div className="box-rating">
                  <h3>
                    Rating: <span>{rating.average}</span>
                  </h3>
                  <Rating rating={rating.average} />{" "}
                </div>
              </>
            )}
          </div>
        </div>
        <div className="movie__info--block">
          <h2>Summary</h2>
          <div
            dangerouslySetInnerHTML={summaryDiv()}
            className="movie__summary"
          />
        </div>
        <div className="movie__info--block">
          <h2>Cast</h2>
          <div className="cast-container">
            <Cast casts={casts} />
          </div>
        </div>
      </section>
    </>
  );
};

export default Movie;
