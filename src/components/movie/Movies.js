import React, { useEffect } from "react";
import MovieCard from "./MovieCard";

const Movies = ({ shows }) => {

  function removeDuplicate(arr) {
    let tempArray = arr.map((element) => {
      return element.show;
    });
    const data = Array.from(new Set(tempArray.map(JSON.stringify))).map(
      JSON.parse,
    );
    return data.sort((firstFilm, secondeFilm) => secondeFilm.rating.average - firstFilm.rating.average);
  }

  return (
    <div className="card-container">
      {removeDuplicate(shows).map((show) => (
        <MovieCard key={show.id} show={show} />
      ))}
    </div>
  );
}

export default Movies;