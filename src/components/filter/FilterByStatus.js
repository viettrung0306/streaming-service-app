import React from "react";

const FilterByStatus = ({ setFilterByStatus }) => {
  const handleChange = (e) => {
    console.log(e.target.value);
    setFilterByStatus(e.target.value);
  };

  return (
    <>
      <select
        id="show-genre"
        className="form-control"
        name="Show[genre]"
        onChange={handleChange}
      >
        <option value="">Show Status</option>
        <option value="Running">Running</option>
        <option value="Ended">Ended</option>
        <option value="To Be Determined">To Be Determined</option>
        <option value="In Development">In Development</option>
      </select>
    </>
  );
};

export default FilterByStatus;
