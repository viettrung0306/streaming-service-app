import React, { useContext, useState } from "react";
import { FavoritesContext } from "../favorite/Favorite";
import { useNavigate } from "react-router-dom";
import "./Nav.scss";

const Nav = ({ children, setShowsFavorite }) => {
  const { favorites } = useContext(FavoritesContext);
  let navigate = useNavigate();

  const showFavoritesMovie = () => {
    console.log(favorites);
  };

  const onClickLogoHandler = () => {
    navigate("/browse");
  };

  const onClickFavoritHandler = () => {
    console.log(favorites);
    navigate('/profile');
  };

  const onLogOut = () => {
    console.log("Log Out");
    localStorage.removeItem('accessToken');
    navigate('/');
  }

  return (
    <div className="nav false">
      <div onClick={onClickLogoHandler}>
        <img
          className="nav__logo"
          src="https://www.freepnglogos.com/uploads/netflix-logo-0.png"
          alt="logo"
        />
      </div>
      <div className="filter">{children}</div>
      <div className="dropdown">
        <div onClick={showFavoritesMovie} className="dropdown-btn">
          <img
            className="nav__avatar"
            src="http://pngimg.com/uploads/netflix/netflix_PNG8.png"
            alt="avatar"
          />
        </div>
        <div className="dropdown-content">
          <ul>
            <li onClick={onClickFavoritHandler}>My Favorites</li>
            <li onClick={onLogOut}>Log Out</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Nav;
