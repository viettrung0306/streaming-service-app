import React from "react";

const SearchBox = ({ setSearchValue }) => {
  return (
    <input
      type="text"
      className="form-control"
      onChange={(event) => setSearchValue(event.target.value)}
      placeholder="Type to search..."
    />
  );
};

export default SearchBox;
