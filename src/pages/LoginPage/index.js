import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "./Login.scss";

export default function LoginPage() {
  let navigate = useNavigate();
  
  useEffect(() => {
    localStorage.getItem('accessToken') === 'true' && navigate('/browse');
  }, []);
  
  const userInfo = {
    userID: "hvtrung@gmail.com",
    password: "123456",
  };

  function validateEmail() {
    const phoneFormat = /^[0]\d{9}$/;
    const emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const input = document.getElementById("username");
    const inputText = input.value;
    const messageDiv = document.getElementById("message");

    console.log(inputText);
    if (inputText.match(phoneFormat) || inputText.match(emailFormat)) {
      console.log("Match");
      messageDiv.innerHTML = "";
    } else {
      console.log("Not Match");
      input.classList.add("invalidInput");
      messageDiv.innerHTML = "Please input the correct phone number";
      messageDiv.classList.add("messageError");
    }
  }

  function verifyLogin(id, pwd) {
    let { userID, password } = userInfo;
    console.log("pwd: ", pwd, " id: ", id);
    if (id === userID && pwd === password) {
      console.log("Login Success");
      localStorage.setItem("accessToken", true);
      localStorage.setItem("favorite", '');
      navigate("/browse");
    } else {
      console.log("Fail");
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(e.target.elements);
    const { username, password } = e.target.elements;
    verifyLogin(username.value, password.value);
  };
  return (
    <section className="login-Page">
      <form className="login" onSubmit={handleSubmit}>
        <h1 className="login__title">Sign In</h1>
        <div className="login__group">
          <input
            className="login__group__input"
            type="text"
            name="username"
            id="username"
            required
            onKeyUp={validateEmail}
          />
          <label className="login__group__label move-label">
            Email or phone number
          </label>
          <div id="message"></div>
        </div>
        <div className="login__group">
          <input
            className="login__group__input"
            type="password"
            name="password"
            required
          />
          <label className="login__group__label move-label">Password</label>
        </div>
        <button className="login__sign-in" type="submit">
          Sign In
        </button>
      </form>
    </section>
  );
}
