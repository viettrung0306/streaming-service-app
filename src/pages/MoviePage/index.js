import axios from "axios";
import React, { useEffect, useState } from "react";
import Movie from "../../components/movie/Movie";
import Nav from "../../components/nav/Nav";
import API_URL from "../../constants/Constants";
import "./MoviePage.scss";

const MoviePage = () => {
  const [movie, setMovie] = useState();

  useEffect(() => {
    getMovie();
  }, []);

  async function getMovie() {
    let id = localStorage.getItem("id");
    const res = await axios.get(`${API_URL}/shows/${id}`);
    setMovie(res.data);
    console.log(res.data);
  }

  return (
    <>
      <Nav></Nav>
      {movie && <Movie movie={movie} />}
    </>
  );
};

export default MoviePage;
