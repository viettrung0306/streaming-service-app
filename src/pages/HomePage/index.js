import React, { useEffect, useState } from "react";
import axios from "axios";
import Nav from "../../components/nav/Nav";
import Movies from "../../components/movie/Movies";
import API_URL from "../../constants/Constants";
import "./HomePage.scss";
import SearchBox from "../../components/searchbox/Searchbox";
import Filter from "../../components/filter/Filter";
import FilterByStatus from "../../components/filter/FilterByStatus";

export default function HomePage() {
  const [shows, setShows] = useState([]);
  const [showsFavorite, setShowsFavorite] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [filterByGenre, setFilterByGenre] = useState("");
  const [filterByStatus, setFilterByStatus] = useState("");

  useEffect(() => {
    fetchDataBySchedule();
  }, []);

  useEffect(() => {
    getMovie(searchValue);
  }, [searchValue]);

  useEffect(() => {
    fetchDataByGenre(filterByGenre);
  }, [filterByGenre]);

  useEffect(() => {
    fetchDataByStatus(filterByStatus);
  }, [filterByStatus]);

  async function fetchDataByGenre(filterByGenre) {
    if (filterByGenre.length !== 0) {
      try {
        const response = await axios.get(`${API_URL}/schedule/full`);
        console.log("Res: ", response);
        const movies = response.data;
        const temp = new Set();

        // let runningShows = movies.filter((movie) => movie.genres.includes(filterByGenre));
        let runningShows = movies.filter((item) => {
          let { show } = item._embedded;
          return show.genres.includes(filterByGenre);
        });
        let demo = runningShows.map((show) => {
          const duplicate = temp.has(show._embedded.id);
          temp.add(duplicate);
          return !duplicate && show._embedded;
        });
        console.log("running shows: ", runningShows);
        setShows(demo);
      } catch (error) {
        console.error(error);
      }
    } else {
      fetchDataBySchedule();
    }
  }

  async function fetchDataByStatus(filterByStatus) {
    if (filterByStatus.length !== 0) {
      try {
        const response = await axios.get(`${API_URL}/schedule/full`);
        console.log("Res: ", response);
        const movies = response.data;
        const temp = new Set();

        // let runningShows = movies.filter((movie) => movie.genres.includes(filterByGenre));
        let runningShows = movies.filter((item) => {
          let { show } = item._embedded;
          return show.status === filterByStatus;
        });
        let demo = runningShows.map((show) => {
          const duplicate = temp.has(show._embedded.id);
          temp.add(duplicate);
          return !duplicate && show._embedded;
        });
        console.log("running shows: ", runningShows);
        setShows(demo);
      } catch (error) {
        console.error(error);
      }
    } else {
      fetchDataBySchedule();
    }
  }

  async function fetchDataBySchedule() {
    try {
      // const response = await axios.get(`${API_URL}/shows`);
      const response = await axios.get(`${API_URL}/schedule/full`);

      console.log("Schedule: ", response);
      const items = response.data;
      // Popular
      const temp = new Set();

      let runningShows = items.filter((item) => {
        let { show } = item._embedded;
        return (
          (show.weight >= 99) &
          (show.type === "Scripted") &
          (show.rating.average >= 6.0)
        );
      });

      let demo = runningShows.map((show) => {
        const duplicate = temp.has(show._embedded.id);
        temp.add(duplicate);
        return !duplicate && show._embedded;
      });

      // removeDuplicate(runningShows);
      console.log("Upcoming shows: ", demo);
      setShows(demo);
    } catch (error) {
      console.error(error);
    }
  }

  const getMovie = async (searchValue) => {
    if (searchValue.length !== 0) {
      const response = await axios(`${API_URL}/search/shows?q=${searchValue}`);
      setShows(response.data);
    } else {
      fetchDataBySchedule();
    }
  };

  return (
    <>
      <Nav setShowsFavorite={setShowsFavorite}>
        <Filter setFilterByGenre={setFilterByGenre} />
        <FilterByStatus setFilterByStatus={setFilterByStatus} />
        <SearchBox setSearchValue={setSearchValue} />
      </Nav>
      <>
        <Movies shows={shows}></Movies>
      </>
    </>
  );
}
