import React, { useContext, useState } from "react";
import Nav from "../../components/nav/Nav";
import { FavoritesContext } from "../../components/favorite/Favorite";
import MovieCard from "../../components/movie/MovieCard";
import "./Profile.scss";
import _ from "lodash";

const ProfilePage = () => {
  const { favorites, add1 } = useContext(FavoritesContext);
  const [favorite, setFavorite] = useState(true);

  function summaryDiv(summary) {
    return { __html: `${summary}` };
  }

  const handlerAddFavorite = (movie) => {
    add1(movie);
    setFavorite(!favorite);
  };

  return (
    <>
      <Nav />
      <div className="favorites-list">
        <h1>My Favorites: ({favorites.length} films)</h1>
        {favorites.map((fav) => (
          <MovieCard show={fav}>
            <div className="movie__infos">
              <div>Genres: {fav.genres.toString()}</div>
              <div>Status: {fav.status}</div>
              <div>Rating: {fav.rating.average}</div>
            </div>

            <div>
              <h2>Summary</h2>
              <div
                dangerouslySetInnerHTML={summaryDiv(fav.summary)}
                className="movie__summary"
              />
            </div>

            <button className="add-btn" onClick={() => handlerAddFavorite(fav)}>
              Remove from Favorites
            </button>
          </MovieCard>
        ))}
      </div>
    </>
  );
};

export default ProfilePage;
